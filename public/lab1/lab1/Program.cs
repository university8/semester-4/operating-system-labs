﻿using System;
using System.Linq;
using System.Threading;

namespace lab1 {
  public class ThreadMultiplication {
    public static int[] GetVector(int length) {
      Random rnd = new Random();
      int[] arr = new int[length];

      for (int i = 0; i < length; i++) {
        arr[i] = rnd.Next(1,10);
      }
      return arr;
    }
    public static int[,] GetMatrix(int length) {
      Random rnd = new Random();
      int[,] matrix = new int[length,length];
      for (int i = 0; i < length; i++) {
        for (int j = 0; j < length; j++) {
          matrix[i, j] = rnd.Next(1,10);
        }
      }
      return matrix;
    }
    public static int[] MultiplicationWithThreads(int[,] matrix, int[] vector, int length, int threadsCount){
      int[] result = new int[length];
      int step = 0;
      Thread[] threads = new Thread[threadsCount];
      for (int i = 0; i < threadsCount; i++){
        int[] res = result;
        threads[i] = new Thread(() =>{
            int core = step++;
            for (int index = core * length / threadsCount; index < (core + 1) * length / threadsCount; index++){
              for (int j = 0; j < length; j++){
                res[index] += matrix[index, j] * vector[j];
              }
            }
        });
      }
      for (int i = 0; i < threadsCount; i++){
        threads[i].Start();
      }
      for (int i = 0; i < threadsCount; i++){
        threads[i].Join();
      }
      return result;
    }

    public static int[] MultiplicationWithPLinq(int[,] matrix, int[] vector, int length, int threadsCount) {
      int[] result = new int[length];
      
      var source = Enumerable.Range(0, length);
      var pquery = from num in source.AsParallel().WithDegreeOfParallelism(threadsCount)
                                 select num;
      pquery.ForAll((index) =>{
        int sum = 0;
        for (int j = 0; j < length; j++) {
            sum += matrix[index,j] * vector[j];
        }
        result[index] = sum;
      });
      return result;
    }

    public static void Main(string[] args){ }
  }
}