using System;
using lab1;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using NUnit.Framework;

namespace lab1_test {
  public class Tests {
    [Test]
    public void Test_function_MultiplicationWithThreads_v1() {
      int[,] matrix = {{2, 3},{4, 5}};
      int[] vector = {3,5};
      int[] answer = {21,37};
      Assert.AreEqual(answer,
        ThreadMultiplication.MultiplicationWithThreads(matrix,vector,2,2));
    }
    [Test]
    public void Test_function_MultiplicationWithPLinq_v1() {
      int[,] matrix = {{2, 3},{4, 5}};
      int[] vector = {3,5};
      int[] answer = {21,37};
      Assert.AreEqual(answer,
        ThreadMultiplication.MultiplicationWithPLinq(matrix,vector,2,2));
      
    }
    [Test]
    public void Test_function_MultiplicationWithThreads_v2() {
      var time = new System.Diagnostics.Stopwatch();
      int[,]matrix = ThreadMultiplication.GetMatrix(12);
      int[] vector= ThreadMultiplication.GetVector(12);
      time.Start();
      ThreadMultiplication.MultiplicationWithThreads(matrix,vector,12,2);
      time.Stop();
      var elapsedMs1 = time.ElapsedMilliseconds;
      time.Start();
      ThreadMultiplication.MultiplicationWithThreads(matrix,vector,12,6);
      time.Stop();
      var elapsedMs2 = time.ElapsedMilliseconds;
      Assert.Less(elapsedMs1, elapsedMs2);
    }
    [Test]
    public void Test_function_MultiplicationWithPLinq_v2() {
      var time = new System.Diagnostics.Stopwatch();
      int[,]matrix = ThreadMultiplication.GetMatrix(12);
      int[] vector= ThreadMultiplication.GetVector(12);
      time.Start();
      ThreadMultiplication.MultiplicationWithPLinq(matrix,vector,12,2);
      time.Stop();
      var elapsedMs1 = time.ElapsedMilliseconds;
      time.Start();
      ThreadMultiplication.MultiplicationWithPLinq(matrix,vector,12,6);
      time.Stop();
      var elapsedMs2 = time.ElapsedMilliseconds;
      Assert.Less(elapsedMs1, elapsedMs2);
    }
  }
}